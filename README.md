# 支付教程案例
#### 教程主要分成两组件项目：
1. 基础组件 [pay-java-parent](https://gitee.com/egzosn/pay-java-parent)

    全能第三方支付对接Java开发工具包.优雅的轻量级支付模块集成支付对接支付整合（微信,支付宝,银联,友店,富友,跨境支付paypal,payoneer(P卡派安盈)易极付）app,扫码,网页支付刷卡付条码付刷脸付转账服务商模式、支持多种支付类型多支付账户，支付与业务完全剥离，简单几行代码即可实现支付，简单快速完成支付模块的开发，可轻松嵌入到任何系统里 目前仅是一个开发工具包（即SDK），只提供简单Web实现，建议使用maven或gradle引用本项目即可使用本SDK提供的各种支付相关的功能 
    #### 特性
       1. 不依赖任何 mvc 框架，依赖极少:httpclient，fastjson,log4j,com.google.zxing，项目精简，不用担心项目迁移问题
       2. 也不依赖 servlet，仅仅作为工具使用，可轻松嵌入到任何系统里（项目例子利用spring mvc的 @PathVariable进行，推荐使用类似的框架）
       3. 支付请求调用支持HTTP和异步、支持http代理，连接池
       4. 简单快速完成支付模块的开发
       5. 支持多种支付类型多支付账户扩展 
2. spring boot组件[pay-spring-boot-starter](https://gitee.com/egzosn/pay-spring-boot-starter-parent)

    pay-spring-boot-starter 是一个基于spring-boot实现自动化配置的支付对接， 让你真正做到一行代码实现支付聚合， 让你可以不用理解支付怎么对接，只需要专注你的业务

    #### 特性
        
         1. 项目第三方依赖极少，依托于spring boot与pay-java，项目精简，不用担心项目迁移问题
         2. 一行代码解决配置，一行代码发起支付，一行代码处理回调并且业务与支付完全隔离
         3. 项目扩展性极强极灵活，组件中暴露大量接口，实现对应接口重写加入spring容器即可覆盖全部功能
         4. 引入pay-java具体支付组件即可激活某一支付功能，代码可以不用任何修改即可使用
 ---
#### spring boot组件在以下代码托管网站

    码云：https://gitee.com/egzosn/pay-spring-boot-starter-parent
    GitHub：https://github.com/egzosn/pay-spring-boot-starter-parent

#### 基础组件 pay-java-parent 

	码云：https://gitee.com/egzosn/pay-java-parent
    GitHub：https://github.com/egzosn/pay-java-parent
    
    
#### 教程讲解
   1.  基础组件pay-java-parent
        * [第三方对接-支付宝支付接入教程](https://my.oschina.net/cnzzs/blog/3041446) 
   
   2.  spring boot组件pay-spring-boot-starter
        * [快速入门篇](https://my.oschina.net/cnzzs/blog/3154953)
            * [内存案例源码](./spring-boot/quick-start/memory)
            * [Jdbc案例源码](./spring-boot/quick-start/memory)
